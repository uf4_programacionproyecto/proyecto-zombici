package armas;

import java.util.ArrayList;

import humanoides.Zombie;

public class Hacha extends Armas {

	public Hacha(String nombreArma, int dañoArma, int alcanceArma, int aciertoArma) {
		super(nombreArma, dañoArma, alcanceArma, aciertoArma);

	}

	// habilidad especial sobreescrita
	@Override
	public String toString() {
		return "Mata gratis a 1 gordo";
	}

	public void habilidadEspecial(ArrayList<Zombie> zombiesPartida) {
		boolean gordos = false;

		// Identificar a Gordo vivo y quitarlo de la lista de zombies de la partida
		for (int i = 0; i < zombiesPartida.size(); i++) {
			Zombie zombie = zombiesPartida.get(i);

			if (zombie.getTipo().equals("Gordo")) {
				gordos = true;

				zombie.setSalud(0);
				zombie.setEstaVivo(false);
				zombiesPartida.remove(i);
				System.out.println("Has eliminado a " + zombie.getNombre() + " con el Hacha.");
				break;
			}
		}
		// Mensaje de fallo de la habilidad especial
		if (gordos == false) {
			System.out.println("No se ha podido hacer uso de la habilidad especial del arma");
		}
	}
}