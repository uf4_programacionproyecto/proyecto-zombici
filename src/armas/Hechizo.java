package armas;

import java.util.ArrayList;

import humanoides.Zombie;

public class Hechizo extends Armas {
	
	public Hechizo (String nombreArma, int dañoArma, int alcanceArma, int aciertoArma) {
		super (nombreArma, dañoArma, alcanceArma, aciertoArma);
		
	}
	
	//habilidad especial sobreescrita 
	@Override
	public String toString() {
		return "Mata gratis a 2 caminantes";
	}
	public void HabilidadEspecial(ArrayList<Zombie> zombiesPartida) {
		int caminantesEliminados = 0;
		boolean caminante = false;
		
		// Identificar los caminantes vivos y quitarlos de la lista de zombies de la parida
		for (int i = 0; i < zombiesPartida.size(); i++) {
			Zombie zombie = zombiesPartida.get(i);
			
			if (zombie.getTipo().equals("Caminante")){
				caminante = true;
				
				zombie.setSalud(0);
				zombie.setEstaVivo(false);
				zombiesPartida.remove(i); 
				System.out.println("Has eliminado a un caminante con el Hechizo.");
				caminantesEliminados++;
				
				if (caminantesEliminados == 2) {
					break;
				}
            }
		}
		//Mensaje de fallo de la habilidad especial
		if (caminante == false) {
			System.out.println("No se ha podido hacer uso de la habilidad especial del arma");
		}
	}

}
