package armas;

public class Armas {
	
	//atributos globales
	private String nombreArma;
	private int dañoArma;
	private int alcanceArma;
	private int aciertoArma;
	
	//constructor
	public Armas(String nombreArma, int dañoArma, int alcanceArma, int aciertoArma) {
		setNombreArma(nombreArma);
		setDañoArma(dañoArma);
		setAlcanceArma(alcanceArma);
		setAciertoArma(aciertoArma);
	}
	
	//constructor vacio para arma basica
	public Armas() {
		setNombreArma("Daga");
		setDañoArma(1);
		setAlcanceArma(1);
		setAciertoArma(4);
	}
	
	//getters y setters
	public String getNombreArma() {
		return nombreArma;
	}
	
	public void setNombreArma (String nombreArma) {
		this.nombreArma = nombreArma;
	}
	
	public int getDañoArma() {
		return dañoArma;
	}
	
	public void setDañoArma (int dañoArma) {
		this.dañoArma = dañoArma;
	}
	
	public int getAlcanceArma() {
		return alcanceArma;
	}
	
	public void setAlcanceArma (int alcanceArma) {
		this.alcanceArma = alcanceArma;
	}
	
	public int getAciertoArma() {
		return aciertoArma;
	}
	
	public void setAciertoArma(int aciertoArma) {
		this.aciertoArma = aciertoArma;
	}
	
	//habilidad especial a sobreescribir
	public String habilidadEspecialArma () {
		return "No hay habilidad";
	}
	
}
