package armas;

import java.util.ArrayList;

import humanoides.Zombie;

public class Arco extends Armas {

	//constructor
	public Arco(String nombreArma, int dañoArma, int alcanceArma, int aciertoArma) {
		super(nombreArma, dañoArma, alcanceArma, aciertoArma); //atributos heredados

	}

	//habilidad especial sobreescrita 
	@Override
	public String toString() {
		return "Mata gratis a 1 corredor";
	}

	public void HabilidadEspecial(ArrayList<Zombie> zombiesPartida) {
		boolean corredores = false;
		
		// Identificar a corredor viv yo quitarlo a la lista de zombies de la partida
		for (int i = 0; i < zombiesPartida.size(); i++) {
			Zombie zombie = zombiesPartida.get(i);
			if (zombie.getTipo().equals("Corredor")) {
				corredores = true;
				zombie.setSalud(0);
				zombie.setEstaVivo(false);
				zombiesPartida.remove(i);
				System.out.println("Has eliminado a " + zombie.getNombre() + " con el Arco.");
				break;

			}
		}
		//Mensaje de fallo de la habilidad especial
		if (corredores == false) {
			System.out.println("No se ha podido hacer uso de la habilidad especial del arma");
		}
	}

}
