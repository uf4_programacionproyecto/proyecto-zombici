package armas;

import java.util.ArrayList;
import java.util.Random;

import humanoides.Zombie;
public class Espada extends Armas {
	
	public Espada (String nombreArma, int dañoArma, int alcanceArma, int aciertoArma) {
		super (nombreArma, dañoArma, alcanceArma, aciertoArma);
		
	}
	
	//habilidad especial sobreescrita 
	@Override
	public String toString() {
		return "Mata gratis a 2 zombies aleatorios";
	}
	
	public void habilidadEspecial(ArrayList<Zombie> zombiesPartida) {
	    Random scanner = new Random();
	    int zombiesEliminados = 0;

	 // Identificar zombies vivos y quitarlos de la lista de zombies de la partida
	    while (zombiesEliminados < 2 && !zombiesPartida.isEmpty()) {
	        int indiceZombie = scanner.nextInt(zombiesPartida.size());
	        
	        Zombie zombieEliminado = zombiesPartida.remove(indiceZombie);
	        zombieEliminado.setSalud(0);
	        zombieEliminado.setEstaVivo(false);
	        
	        System.out.println("Has eliminado a " + zombieEliminado.getNombre() + " con la Espada.");
	        zombiesEliminados++;
	        
	    }
	}
}
