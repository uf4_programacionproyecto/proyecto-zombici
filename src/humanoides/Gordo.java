package humanoides;

public class Gordo extends Zombie {

	public Gordo(String nombre, int salud, int max_salud, boolean estaVivo, int movimiento, int daño, String tipo) {
		super(nombre, salud, max_salud, estaVivo, movimiento, daño, "Gordo");

	}

	//habilidad especial sobreescrita
	@Override
	public String toString() {
		return "\nHABILIDAD ESPECIAL (Gordo): Se elimina a otro gordo";

	}

	public void HabilidadEspecialZombie(ArrayList<Zombie> zombiesPartida) {
		boolean gordosVivos = false;
		ArrayList<Zombie> gordosVivosLista = new ArrayList<>(); //nueva lista de Gordos vivos
	    
	    // Identificar los gordos vivos y agregarlos a la lista de gordos vivos
	    for (Zombie zombie : zombiesPartida) {
	        if (zombie instanceof Gordo && zombie.getEstaVivo()) {
	            gordosVivos = true;
	            gordosVivosLista.add(zombie);
	        }
	    }
	    
	    // Eliminar un Gordo vivo aleatorio de zombiesPartida, si existen + Mostrar mensajes según el resultado de la habilidad especial
	    if (gordosVivos) {
	        int indiceGordoAleatorio = (int) (Math.random() * gordosVivosLista.size()); //num aleatorio de gordo a eliminar
	        Zombie gordoAEliminar = gordosVivosLista.get(indiceGordoAleatorio);
	        gordoAEliminar.setSalud(0);
	        gordoAEliminar.setEstaVivo(false);
	        zombiesPartida.remove(gordoAEliminar); //quitar del array de zombies de la partida
	        System.out.println("Se ha eliminado a " + gordoAEliminar.getNombre());
	    } else {
	        System.out.println("No hay gordos vivos para eliminar");
	    }
		
	}
}
