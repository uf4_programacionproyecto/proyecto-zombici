package humanoides;

public class Zombie extends Humanoide {

	// atributos globales
	private int movimiento;
	private int daño;
	private String tipo;

	// constructor
	public Zombie(String nombre, int salud, int max_salud, boolean estaVivo, int movimiento, int daño, String tipo) {
		super(nombre, salud, max_salud, estaVivo); // atributos heredados
		setMovimiento(movimiento);
		setDaño(daño);
		setTipo(tipo);

	}

	// getters y setters
	public int getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(int movimiento) {
		this.movimiento = movimiento;
	}

	public int getDaño() {
		return daño;
	}

	public void setDaño(int daño) {
		this.daño = daño;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	//metodo habilidad especial a sobreescribir
	public String toString() {
		return "Falta determinar la habilidad especial";
	}

}
