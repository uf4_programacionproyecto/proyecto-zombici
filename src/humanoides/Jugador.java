package humanoides;

import armas.Armas;

public class Jugador extends Humanoide {

	// atributos globales
	private Armas arma;
	private boolean pasaNivel;
	private boolean habilidadEspecialActivada;

	// constructor
	public Jugador(String nombre, int max_salud, int salud, boolean estaVivo, Armas arma) {
		super(nombre, max_salud, salud, estaVivo); //atributos heredados
		setArma(arma);
		setPasaNivel(false);
		setHabilidadEspecialActivada(false);
	}

	//getters y setters
	public Armas getArma() {
		return arma;
	}

	public void setArma(Armas arma) {
		this.arma = arma;
	}

	public boolean getPasaNivel() {
		return pasaNivel;
	}

	public void setPasaNivel(boolean pasaNivel) {
		this.pasaNivel = pasaNivel;
	}

	//metodos Habilidad Especial
	public void habilidadEspecial() {
		arma.habilidadEspecialArma();
	}

	public boolean getHabilidadEspecialActivada() {
		return habilidadEspecialActivada;
	}

	public void setHabilidadEspecialActivada(boolean habilidadEspecialActivada) {
		this.habilidadEspecialActivada = habilidadEspecialActivada;
	}
}