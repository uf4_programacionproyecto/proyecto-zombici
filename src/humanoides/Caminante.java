package humanoides;

public class Caminante extends Zombie {

	//constructor
	public Caminante(String nombre, int salud, int max_salud, boolean estaVivo, int movimiento, int daño, String tipo) {
		super(nombre, salud, max_salud, estaVivo, movimiento, daño, "Caminante"); //atributos heredados

	}

	//habilidad especial sobreescrita 
	@Override
	public String toString() {
		return "\nHABILIDAD ESPECIAL (Caminante): Se dobla a la cantidad de caminantes";

	}

	public void HabilidadEspecialZombie(ArrayList<Zombie> zombiesPartida) {
		int contador_caminantes = 0;

		ArrayList<Zombie> nuevosZombies = new ArrayList<>();
		
	    // Identificar los caminantes vivos y agregarlos a la lista de caminantes a añadir
		for (Zombie zombie : zombiesPartida) {
			if (zombie instanceof Caminante) { 
				nuevosZombies.add(new Caminante("Caminante", 1, 1, true, 2, 1, "Caminante")); //crear nuevo caminante
				contador_caminantes ++;
			}
		}
		//añadir todos los caminantes creados a la lista de zombies en la partida
		zombiesPartida.addAll(nuevosZombies);
		
		// Mostrar mensajes según el resultado de la habilidad especial
		if (contador_caminantes == 0) {
			System.out.println("No hay Caminantes vivos para duplicar");
		} else {
			System.out.println("Se ha duplicado el número de Caminantes vivos");
		}
	}
}
