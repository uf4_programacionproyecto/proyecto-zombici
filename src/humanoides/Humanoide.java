package humanoides;

public class Humanoide {

	// atributos globales
	private String nombre;
	private int salud;
	private int max_salud;
	private boolean estaVivo;

	// constructor
	public Humanoide(String nombre, int max_salud, int salud, boolean estaVivo) {
		setNombre(nombre);
		setSaludMax(max_salud);
		setSalud(salud);
		setEstaVivo(true);

	}

	// getters y setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getSalud() {
		return salud;
	}

	public void setSalud(int salud) {
		this.salud = salud;
	}

	public int getSaludMax() {
		return max_salud;
	}

	public void setSaludMax(int max_salud) {
		this.max_salud = max_salud;
	}

	public boolean getEstaVivo() {
		return estaVivo;
	}

	public void setEstaVivo(boolean estaVivo) {
		this.estaVivo = estaVivo;
	}
}