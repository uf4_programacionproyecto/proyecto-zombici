package humanoides;

public class Corredor extends Zombie {

	public Corredor(String nombre, int salud, int max_salud, boolean estaVivo, int movimiento, int daño, String tipo) {
		super(nombre, salud, max_salud, estaVivo, movimiento, daño, "Corredor");

	}

	//habilidad especial sobreescrita
	@Override
	public String toString() {
		return "\nHABILIDAD ESPECIAL (Corredor): Se elimina a los corredores vivos";

	}

	public void HabilidadEspecialZombie(ArrayList<Zombie> zombiesPartida) {
		boolean corredoresVivos = false;
		ArrayList<Zombie> corredoresAEliminar = new ArrayList<>();
	    
	    // Identificar los corredores vivos y agregarlos a la lista de corredores a eliminar
	    for (Zombie zombie : zombiesPartida) {
	        if (zombie instanceof Corredor && zombie.getEstaVivo()) {
	            corredoresVivos = true;
	            corredoresAEliminar.add(zombie);
	        }
	    }
	    
	    // Eliminar a los corredores vivos de zombiesPartida
	    for (Zombie corredor : corredoresAEliminar) {
	        corredor.setSalud(0);
	        corredor.setEstaVivo(false);
	        zombiesPartida.remove(corredor);
	    }

	    // Mostrar mensajes según el resultado de la habilidad especial
	    if (!corredoresVivos) {
	        System.out.println("No hay corredores vivos para eliminar");
	    } else {
	        System.out.println("Se han eliminado a todos los corredores vivos");
	    }
	}
}
