package main;

import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

import armas.Arco;
import armas.Armas;
import armas.Espada;
import armas.Hacha;
import armas.Hechizo;
import humanoides.Caminante;
import humanoides.Corredor;
import humanoides.Gordo;
import humanoides.Jugador;
import humanoides.Zombie;

public class Partida {

	//arrays globales
	private ArrayList<Jugador> personajesSelec;
	private ArrayList<Armas> armasDisponibles;
	private ArrayList<Zombie> zombies;
	private ArrayList<Zombie> zombiesPartida;
	private ArrayList<Armas> armasJuego;

	//constructor
	public Partida(ArrayList<Jugador> personajesSeleccionados, ArrayList<Armas> armasDisponibles, ArrayList<Armas> armasJuego, ArrayList<Zombie> zombies) {
		setPersonajesSelec(personajesSeleccionados);
		this.armasDisponibles = armasDisponibles;
		this.zombies = zombies;
		this.armasJuego = armasJuego;
		this.zombiesPartida = new ArrayList<>();
	}

	public void setPersonajesSelec(ArrayList<Jugador> personajesSelec) {
		this.personajesSelec = personajesSelec;
	}

	public ArrayList<Jugador> getPersonajesSelec() {
		return personajesSelec;
	}

	// Se inicia el juego segun el num de personajes con los que se juega
	public void iniciarJuego() {
		int nivel = personajesSelec.size();
		jugarNiveles(nivel);
	}

	// Se generan los zombies y se empiezan las rondas
	public void jugarNiveles(int nivel) {

		while (!personajesSelec.isEmpty()) {
			System.out.println("\nN I V E L:  " + nivel);
			generarZombies(nivel);
			jugarRonda(nivel);
			nivel++;
		}

	}

	// Función para generar el num de zombies adecuado para cada nivel
	private void generarZombies(int nivel) {
		zombiesPartida.clear(); // Limpiar la lista de zombies del nivel anterior

		for (int i = 0; i < nivel; i++) {
			Random rand = new Random();
			int indiceZombie = rand.nextInt(zombies.size()); // Genera un índice aleatorio para elegir un zombie

			Zombie zombie = zombies.get(indiceZombie);
			Zombie nuevoZombie;

			// Se crea cada zombie añadido como su subclase
			if (zombie instanceof Corredor) {
				nuevoZombie = new Corredor(zombie.getNombre(), zombie.getSalud(), zombie.getSaludMax(),
						zombie.getEstaVivo(), zombie.getMovimiento(), zombie.getDaño(), zombie.getTipo());
			} else if (zombie instanceof Caminante) {
				nuevoZombie = new Caminante(zombie.getNombre(), zombie.getSalud(), zombie.getSaludMax(),
						zombie.getEstaVivo(), zombie.getMovimiento(), zombie.getDaño(), zombie.getTipo());
			} else if (zombie instanceof Gordo) {
				nuevoZombie = new Gordo(zombie.getNombre(), zombie.getSalud(), zombie.getSaludMax(),
						zombie.getEstaVivo(), zombie.getMovimiento(), zombie.getDaño(), zombie.getTipo());
			} else {
				// Si el tipo de zombie no coincide con ninguna subclase conocida, crea un
				// zombie genérico
				nuevoZombie = new Zombie(zombie.getNombre(), zombie.getSalud(), zombie.getSaludMax(),
						zombie.getEstaVivo(), zombie.getMovimiento(), zombie.getDaño(), zombie.getTipo());
			}

			zombiesPartida.add(nuevoZombie);
		}
	}

	// Función para jugar cada ronda, primero los turnos de los personajes y luegos
	// los turnos de los zombies
	public void jugarRonda(int nivel) {
		int ronda = 1;

		// se juega la misma ronda mientras haya personajes
		while (!personajesSelec.isEmpty() && !zombiesPartida.isEmpty()) {

			System.out.println("\nA T A C A N   L O S   P E R S O N A J E S");
			for (Jugador jugador : personajesSelec) {
				if (jugador.getEstaVivo() == true) {

					System.out.println("\n----- NIVEL: " + nivel + " - " + ronda + " -----");
					imprimirZombiesVivos();
					System.out.println("JUGADOR: " + jugador.getNombre() + " S:" + jugador.getSalud() + " Arma ["
							+ jugador.getArma().getNombreArma() + " DAÑO:" + jugador.getArma().getDañoArma() + " ALC:"
							+ jugador.getArma().getAlcanceArma() + " ACIER:" + jugador.getArma().getAciertoArma()
							+ "]");

					// se muestra el menu y segun la opcion se llama a una funcion o a otra
					int opcion = menuPartida();
					switch (opcion) {
					case 1:
						atacar(jugador);
						if (zombiesPartida.isEmpty()) {
							return;
						}
						break;
					case 2:
						usarHabilidadEspecial(jugador);
						break;
					case 3:
						buscar();
						break;
					case 4:
						cambiarArma(jugador);
						break;
					case 5:
						Juego.nuevoJuego(); // EXTRA: acabar partida y volver al menu principal
					case 6:
						System.out.println("H A S T A   L A   P R Ó X I M A  ! ! !"); // EXTRA: acabar partida y salir
																						// del juego
						System.exit(0);
					case 0:
						break;
					default:
						System.out.println("Opción no válida, por favor intenta de nuevo.");
					}
				}

			}
			System.out.println("\nA T A C A N   L O S   Z O M B I E S");
			for (Zombie zombies : zombiesPartida) {

				if (zombies.getEstaVivo() == true) {
					System.out.println("\n------ NIVEL: " + nivel + " - " + ronda + " ------");
					imprimirPersonajesVivos();
					System.out.println("ZOMBIE: " + zombies.getNombre() + " S:" + zombies.getSalud() + " MOV: "
							+ zombies.getMovimiento() + " DAÑO:" + zombies.getDaño());

					// bucle segun el num de ataques que tiene el zombie
					for (int i = 0; i < zombies.getMovimiento(); i++) {
						// seleccionar un zombie de la partida aleatorio
						Random rd = new Random();
						int indice_p_atacado = rd.nextInt(personajesSelec.size());

						Jugador personaje_aleatorio = personajesSelec.get(indice_p_atacado);

						// opcion eliminar personaje
						if (zombies.getDaño() >= personaje_aleatorio.getSalud()) {
							personajesSelec.get(indice_p_atacado).setSalud(0);
							personajesSelec.get(indice_p_atacado).setEstaVivo(false);
							personajesSelec.remove(indice_p_atacado);
							System.out.println(personaje_aleatorio.getNombre() + " ha muerto");

							// comprobamos si quedan personajes vivos
							if (personajesSelec.isEmpty()) {
								System.out.println("\nE N D   G A M E . . .");
								System.exit(0);
							}

						// opcion solo atacar al persoanaje	
						} else { 
							personajesSelec.get(indice_p_atacado).setSalud(personajesSelec.get(indice_p_atacado).getSalud() - zombies.getDaño());
							System.out.println("Ataque " + (i + 1) + "/" + zombies.getMovimiento() + ": "+ personajesSelec.get(indice_p_atacado).getNombre() + ". Salud: "+ personajesSelec.get(indice_p_atacado).getSalud());
						}

					}

				}

			}
			//al finalizar todos los turnos, si hay zombies/personajes vivos se pasa de ronda
			ronda++;
		}

	}

	//función para imprimir zombies vivos en 1 linia
	public void imprimirZombiesVivos() {

		StringBuilder zombiesVivos = new StringBuilder();
		zombiesVivos.append("==| ");

		for (Zombie zombie : zombiesPartida) {
			if (zombie.getEstaVivo()) {
				zombiesVivos.append(zombie.getNombre()).append(" ");
			}
		}

		if (zombiesVivos.length() > 0) {
			System.out.println(zombiesVivos.substring(0, zombiesVivos.length() - 1) + " |==");
		}

	}
	
	//función para imprimir personajes vivos en 1 linia
	public void imprimirPersonajesVivos() {

		StringBuilder personajesVivos = new StringBuilder();
		personajesVivos.append("==| ");

		for (Jugador jugador : personajesSelec) {
			if (jugador.getEstaVivo()) {
				personajesVivos.append(jugador.getNombre()).append(" ");
			}
		}

		if (personajesVivos.length() > 0) {
			System.out.println(personajesVivos.substring(0, personajesVivos.length() - 1) + " |==");
		}

	}

	//mostrar menu
	public int menuPartida() {
		Scanner leer = new Scanner(System.in);

		System.out.println("1- Atacar");
		System.out.println("2- Habilidad especial");
		System.out.println("3- Buscar");
		System.out.println("4- Cambiar arma");
		System.out.println("5- Volver a menú principal");
		System.out.println("6- Salir del juego");
		System.out.println("0- Pasar");
		System.out.print("Elige una opción: ");

		int opcion = leer.nextInt();

		return opcion;

	}


	private void atacar(Jugador jugador) {
		// seleccionar un zombie de la partida aleatorio
		Random rd = new Random();
		int zombie_atacado = rd.nextInt(zombiesPartida.size());
		Zombie zombie_aleatorio = zombiesPartida.get(zombie_atacado);
		System.out.println("\nAtaca a " + zombie_aleatorio.getNombre());

		// Miramos si el arma tiene igual o más daño que la salud del zombie
		if (zombie_aleatorio.getSalud() < jugador.getArma().getDañoArma()) {
			//tirar dado
			int dado_ataque = rd.nextInt(6) + 1;
			//Eliminar zombie
			if (dado_ataque >= jugador.getArma().getAciertoArma()) {
				zombie_aleatorio.setSalud(0);
				zombie_aleatorio.setEstaVivo(false);
				zombiesPartida.remove(zombie_atacado);
				System.out.println("El zombie " + zombie_aleatorio.getNombre() + " ha muerto");

				// Activar Habilidad Especial Zombie eliminado
				int aleat_hab_zombie = rd.nextInt(101);
				if (aleat_hab_zombie >= 95) {
					HabilidadEspecialZombie(zombie_aleatorio);
				}

			//Fallar dado 
			} else {
				System.out.println("Ha fallado el ataque con un " + dado_ataque);
			}
		// El arma no tiene mas daño que la salud - fallar ataque
		} else {
			System.out.println(zombie_aleatorio.getNombre() + " ha esquivado el ataque");
		}

	}

	private void cambiarArma(Jugador jugador) {
		Scanner scanner = new Scanner(System.in);
		
		//imprimir armas disponibles
		System.out.println("\nArmas disponibles:");
		for (int i = 0; i < armasDisponibles.size(); i++) {
			System.out.println((i + 1) + ". " + armasDisponibles.get(i).getNombreArma());
		}
		System.out.print("Selecciona el número correspondiente al arma que deseas equipar: ");
		//seleccionar num de arma deseada
		int armaSeleccionada = scanner.nextInt();
		scanner.nextLine();

		if (armaSeleccionada >= 1 && armaSeleccionada <= armasDisponibles.size()) {
			Armas armaNueva = armasDisponibles.get(armaSeleccionada - 1);
			//seleccionamos como arma para el jugador
			jugador.setArma(armaNueva);
			//Quitamos arma de la lista de disponibles
			armasDisponibles.remove(armaNueva); 
			System.out.println(
					"Se ha cambiado el arma del jugador " + jugador.getNombre() + " por " + armaNueva.getNombreArma());
		} else {
			System.out.println("Arma seleccionada no válida.");
		}
	}

	// buscar aleatoriamente entre las armas que tiene el juego
	private void buscar() {
		Random rd = new Random();
		
		//mensaje en caso de que no hubiera armas
		if (armasJuego.isEmpty()) {
			System.out.println("\nNo hay armas disponibles para buscar.");
			return;
		}

		//num aleatorio de arma 
		int indiceArmaAleatoria = rd.nextInt(armasJuego.size());
		Armas armaEncontrada = armasJuego.get(indiceArmaAleatoria); 															
		System.out.println("\nHas encontrado el arma " + (indiceArmaAleatoria + 1) + ". " + armaEncontrada.getNombreArma());
		
		// se añade el armaEncontrada a la lista de armas disponibles para jugar
		armasDisponibles.add(armaEncontrada); 
	}

	public void usarHabilidadEspecial(Jugador jugador) {
		// Verificar si la habilidad especial del arma ya ha sido activada en esta ronda
		if (jugador.getHabilidadEspecialActivada()) {
			System.out.println("\n¡Ya has activado la habilidad especial del arma en esta ronda!");
			return;
		}
		
		Armas arma = jugador.getArma();
		
		// Activar habilidad especial segun arma actual
		if (arma instanceof Hacha) {
			Hacha hacha = (Hacha) arma;
			System.out.println(hacha);
			hacha.habilidadEspecial(zombiesPartida);
		} else if (arma instanceof Hechizo) {
			Hechizo hechizo = (Hechizo) arma;
			System.out.println(hechizo);
			hechizo.HabilidadEspecial(zombiesPartida);
		} else if (arma instanceof Arco) {
			Arco arco = (Arco) arma;
			System.out.println(arco);
			arco.HabilidadEspecial(zombiesPartida);
		} else if (arma instanceof Espada) {
			Espada espada = (Espada) arma;
			System.out.println(espada);
			espada.habilidadEspecial(zombiesPartida);
		} else {
			System.out.println("\nEl arma equipada no tiene una habilidad especial.");
		}

		// Marcar la habilidad especial del arma como activada para este jugador en esta ronda
		jugador.setHabilidadEspecialActivada(true);
	}

	public void HabilidadEspecialZombie(Zombie zombie_aleatorio) {
		
		// Activar habilidad especial segun zombie eliminado
		if (zombie_aleatorio instanceof Caminante) {
			Caminante caminante = (Caminante) zombie_aleatorio;
			System.out.println(caminante);
			caminante.HabilidadEspecialZombie(zombiesPartida);
		} else if (zombie_aleatorio instanceof Corredor) {
			Corredor corredor = (Corredor) zombie_aleatorio;
			System.out.println(corredor);
			corredor.HabilidadEspecialZombie(zombiesPartida);
		} else if (zombie_aleatorio instanceof Gordo) {
			Gordo gordo = (Gordo) zombie_aleatorio;
			System.out.println(gordo);
			gordo.HabilidadEspecialZombie(zombiesPartida);
		} else {
			System.out.println("El zombie no tiene habilidad especial.");
		}
	}

}