package main;

import java.util.ArrayList;
import java.util.Scanner;

import armas.Armas;
import armas.Arco;
import armas.Hechizo;
import armas.Espada;
import armas.Hacha;

import humanoides.Jugador;
import humanoides.Zombie;
import humanoides.Caminante;
import humanoides.Corredor;
import humanoides.Gordo;

public class Juego {

	//arrays globales
	private static ArrayList<Jugador> personajes = new ArrayList<>();
	private static ArrayList<Zombie> zombies = new ArrayList<>();
	private static ArrayList<Armas> armasDisponibles = new ArrayList<>();
	private static ArrayList<Armas> armasJuego = new ArrayList<>();

	//iniciamos el main con los personajes, armas y zombies necesarios
	public static void main(String[] args) {
		initJugadores();
		initZombie();
		initObjetos();
		
		//iniciamos nuevo juego
		nuevoJuego();
	}
	
	public static void nuevoJuego() {
		Scanner leer = new Scanner(System.in);
		int opcion;
		
		//mostramos menu y seleccionamos opcion
		while (true) {
			menu();
			opcion = leer.nextInt();
			leer.nextLine();

			switch (opcion) {
			case 0:
				System.out.println("Adiós");
				System.exit(0);
			case 1:
				nuevaPartida();

				break;
			case 2:
				nuevoPersonaje();
				break;
			default:
				System.out.println("Carácter no válido, vuelve a intentarlo");
			}
		}
	}

	//mostrar menu por pantalla
	public static void menu() {
		System.out.println("--------------- Bienvenidos a ZOMBICIDE ---------------");

		System.out.println("0- Salir");
		System.out.println("1- Nueva Partida");
		System.out.println("2- Nuevo Personaje");
		System.out.print("Elige una opcion: ");

	}

	//funcion generica para elegir en los menus
		private static int obtenerOpcion() {
			Scanner leer = new Scanner(System.in);
			System.out.print("Introduce una opción: ");
			int opcion = leer.nextInt();
			leer.nextLine();
			return opcion;
		}
	
	//Funcion nueva partida con los personajes pertinentes
	public static void nuevaPartida() {
		Scanner leer = new Scanner(System.in);
		//array para los personajes con los que se jugará la partida
		ArrayList<Jugador> personajesSeleccionados = new ArrayList<>();
		//nuevo objeto partida con los arrays necesarios
		Partida partida = new Partida(personajesSeleccionados, armasDisponibles, armasJuego,zombies);
		//añadir personajes originales al array de personajes para jugar
		personajesSeleccionados.addAll(personajes.subList(0, Math.min(3, personajes.size()))); 
		
		 // Si hay más de 3 personajes creados se pregunta si se quieren añadir mas
		if (personajes.size() > 3) {
	        System.out.println("\n¿Quieres añadir más personajes? Si o No");
	        String masPersonajes = leer.nextLine();
	        
	        //bucle mientras queden personajes para añadir
	        while (masPersonajes.equalsIgnoreCase("Si") && personajesSeleccionados.size() < personajes.size()) {
	            //imprimir lista de personajes adicionales
	        	System.out.println("\nEsta es la lista de los personajes disponibles que tienes para añadir:");
	            for (int i = 3; i < personajes.size(); i++) {
	                System.out.println(i + "- " + personajes.get(i).getNombre());
	            }
	            
	            //selecionar num de personaje a añdir
	            System.out.print("Introduce número del personaje a añadir (0 para pasar a la partida): ");
	            int indicePersonaje = obtenerOpcion();
	            if (indicePersonaje == 0) break;

	            //se añade a la lista de personajes para jugar
	            personajesSeleccionados.add(personajes.get(indicePersonaje));
	            
	         // Verificar si hay más personajes disponibles para añadir
	            if (personajesSeleccionados.size() < personajes.size()) { 
	                System.out.println("¿Quieres añadir más personajes? Si o No");
	                masPersonajes = leer.nextLine();
	            }
	        }
	    }

		// Mostrar los personajes seleccionados en 1 linia una vez finalizamos la seleccion
	    System.out.println("\nEstos son los personajes seleccionados: "); 
		StringBuilder listaPersonajes = new StringBuilder();
		listaPersonajes.append("--[ ");
		for (Jugador personaje : personajesSeleccionados) {
			listaPersonajes.append(personaje.getNombre()).append(" - ");
		}
		if (listaPersonajes.length() > 0) {
			listaPersonajes.setLength(listaPersonajes.length() - 2);
		}
		System.out.println(listaPersonajes+"]--");

		// Actualizar la lista de personajes para jugar en el objeto nueva partida
	    partida.setPersonajesSelec(personajesSeleccionados);
	    //empezamos la partida
	    partida.iniciarJuego();
	}

	

	//funcion crear nuevo personaje
	public static void nuevoPersonaje() {
		Scanner leer = new Scanner(System.in);
		System.out.println("\nCrear personajes");
		
		//establecer max de personajes
		if (personajes.size() >= 10) {
			System.out.println("\nHas alcanzado el número máximo de personajes (10)");
		}
		
		//crear nuevo personaje con atributos y arma por defecto
		System.out.println("Escribe el nombre del nuevo personaje:");
		String nombre = leer.nextLine();

		Armas arma = new Armas();

		//añdir personaje al array de personajes general
		personajes.add(new Jugador(nombre, 5, 5, true, arma));

		System.out.println("Nuevo personaje creado");
	}

	private static void initJugadores() {
		// Creamos los 3 personajes predeterminados
		personajes.add(new Jugador("James", 7, 7, true, new Armas("Mandoble", 2, 1, 4)));
		personajes.add(new Jugador("Marie", 5, 5, true, new Armas()));
		personajes.add(new Jugador("Jaci", 5, 5, true, new Armas()));

	}

	private static void initZombie() {
		// Creamos los 3 tipos de Zombies
		zombies.add(new Corredor("Corredor", 1, 1, true, 2, 1, "Corredor"));
		zombies.add(new Caminante("Caminante", 1, 1, true, 1, 1, "Caminante"));
		zombies.add(new Gordo("Gordo", 2, 2, true, 1, 1, "Gordo"));

	}

	private static void initObjetos() {
		// Creamos los 4 tipos de armas + las que se podrán encontrar en la partida
		armasDisponibles.add(new Hacha("Hacha doble", 2, 1, 3));
		armasDisponibles.add(new Hechizo("Bola de fuego", 1, 3, 4));
		armasDisponibles.add(new Espada("Espada corta", 1, 1, 4));
		armasDisponibles.add(new Arco("Arco Largo", 1, 2, 3));
		armasJuego.add(new Hacha("Segadora Oscura", 3, 1, 1));
		armasJuego.add(new Hechizo("Llama Eterna", 3, 2, 2));
		armasJuego.add(new Espada("Filo de la Luna", 1, 2, 2));
		armasJuego.add(new Arco("Flecha de Fuego Frío", 2, 1, 4));
		armasJuego.add(new Hacha("Hacha de Guerra", 1, 3, 3));
		armasJuego.add(new Hechizo("Niebla Envolvente", 1, 4, 4));
		armasJuego.add(new Espada("Espada de Destrucción", 3, 2, 3));
		armasJuego.add(new Arco("Flecha de la Aurora", 2, 2, 2));

	}

}
